// // Теоретичні питання
// // 1. Як можна сторити функцію та як ми можемо її викликати?
// // Створити функцію можна за допомогою ключового слова function.

// // 2. Що таке оператор return в JavaScript? Як його використовувати в функціях?
// // Оператор return використовуєть для завершення виконання функції.
// // function add(a, b) {
// //   return a + b;
// // }

// // 3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?
// // Параметри - це зміні, які зазначенні у визначеній фукції.Вони використовуються для передачі вхідних даних.
// // Аргументи - це значення, які передаються у визначену функцію. 

// // 4. Як передати функцію аргументом в іншу функцію?
// // function helloWorld() {
// //     console.log("Helo World");
// // }
// // function executeFunction(a) {
// //     a();
// // }
// // executeFunction(helloWorld);


// Практичні завдання
// 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.

let a = +prompt("Enter number A");
let b = +prompt("Enter number B");
function division(a, b) {
    return a / b
}
console.log(division(a,b));

// 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа).
// Якщо користувач ввів не число, запитувати до тих пір, поки не введе число
// - Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати.
// Сюди може бути введено +, -, *, /. Провалідувати отримане значення. Якщо користувач ввів не передбачене значення,
// вивести alert('Такої операції не існує'). Створити функцію, в яку передати два значення та операцію.
// - Вивести у консоль результат виконання функції.

let firstNum = +prompt("Enter number firstNum");
let secondNum = +prompt("Enter number secondNum");

while (isNaN(firstNum) || isNaN(secondNum)) {
    alert("You entered not a number, please try again")
    firstNum = +prompt("Enter number firstNum");
    secondNum = +prompt("Enter number secondNum");
}

let mathOperat = prompt("Enter a operation: +, -, *, /")
while (mathOperat !== "+" && mathOperat !== "-" && mathOperat !== "*" && mathOperat !== "/") {
    mathOperat = prompt("You entered wrond operation, please try again: +, -, *, /")
}

function calculate(num1, num2, operation) {
    switch (operation) {
        case '+':
            return num1 + num2;
        case '-':
            return num1 - num2;
        case '*':
            return num1 * num2;
        case '/':
            return num2 !== 0 ? num1 / num2 : 'You cannot divide by zero!';
        default:
            return 'Unknown operation';
    }
}

const result = calculate(firstNum, secondNum, mathOperat)
console.log(result);


// 3. Опціонально. Завдання:
// Реалізувати функцію підрахунку факторіалу числа.
// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера число, яке введе користувач.
// - За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
// - Використовувати синтаксис ES6 для роботи зі змінними та функціями.

let number = +prompt("Enter a number")
while (isNaN(number) || number === 0) {
    number = +prompt("Enter again a number")
}

const functionFactorial = (enterNum) => {
    if (enterNum === 1) {
        return 1
    } else return enterNum*functionFactorial(enterNum-1);	
}

const factorial = functionFactorial(number);
alert(factorial)

